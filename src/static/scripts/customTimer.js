function startTimer(duration, display, onTick, onComplete) {
  var start = Date.now(),
    diff,
    minutes,
    seconds;
  var activate = true;
  var intervalID;
  function timer() {
    if (activate) {
      activate = !activate;
      diff = duration - (((Date.now() - start) / 1000) | 0);

      minutes = (diff / 60) | 0;
      seconds = diff % 60 | 0;

      minutes = minutes < 10 ? "0" + minutes : minutes;
      seconds = seconds < 10 ? "0" + seconds : seconds;

      display.textContent = minutes + ":" + seconds;

      if (diff <= 0) {
        // start = Date.now() + 1000;
        clearInterval(intervalID);
        onComplete();
      }
      
    } else {
      activate = !activate;
    }
    onTick();
  }
  timer();
  intervalID = setInterval(timer, 500);
}