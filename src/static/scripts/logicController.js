function guessProcessor(answerStr, guessStr) {
  if (answerStr === guessStr) {
    console.log('It\'s the same thing!');
    return this;
  } else if (answerStr.includes(guessStr)) {
    console.log('It\'s almost the same!'); 
    console.log('The correct answer is:');
    return answerStr;
  } else if (answerStr !== guessStr) {
    console.log('It isn\'t the same');
    console.log('The correct answer is:');
    return answerStr;
  }
}