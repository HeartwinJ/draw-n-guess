$('#redButton').on('click', () => {
  cfd.setDrawingColor([220, 53, 69]);
});

$('#blueButton').on('click', () => {
  cfd.setDrawingColor([0, 123, 255]);
});

$('#greenButton').on('click', () => {
  cfd.setDrawingColor([40, 167, 69]);
});

$('#yellowButton').on('click', () => {
  cfd.setDrawingColor([255, 193, 7]);
});

$('#blackButton').on('click', () => {
  cfd.setDrawingColor([33, 37, 41]);
});

$('#whiteButton').on('click', () => {
  cfd.setDrawingColor([255, 255, 255]);
});

$('#undoButton').on('click', () => {
  cfd.undo();
});

$('#redoButton').on('click', () => {
  cfd.redo();
});

$('#clearButton').on('click', () => {
  cfd.clear();
});

$('#pencilButton').on('click', () => {
  if (cfd.isBucketToolEnabled) {
    cfd.toggleBucketTool();
  }
});

$('#bucketButton').on('click', () => {
  if (!cfd.isBucketToolEnabled) {
    cfd.toggleBucketTool();
  }
});