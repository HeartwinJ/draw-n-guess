var express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var pathComp = require("express-static");

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/front.html');
});

app.get('/game', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

app.use(pathComp(__dirname + '/static'));

io.on('connection', (socket) => {
  console.log('A Player Connected!');
  socket.on('disconnect', () => {
    console.log('A Player Disconnected!')
  });
  socket.on('chat message', (msg) => {
    io.emit('chat message', msg);
  });
  socket.on('redraw', (data) => {
    io.emit('redraw', data);
  });
});

http.listen(3000, () => {
  console.log('listening on *:3000');
});